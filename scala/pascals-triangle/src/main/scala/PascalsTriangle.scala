object PascalsTriangle {
  def pascal(r: Int, c: Int): Int = {
    if (r == c || c == 1) 1
    else pascal(r - 1, c) + pascal(r - 1, c - 1)
  }

  def rows(n: Int): List[List[Int]] = (1 to n).toList
    .map(r => (1 to r).toList
      .map(c => pascal(r, c)))

}
