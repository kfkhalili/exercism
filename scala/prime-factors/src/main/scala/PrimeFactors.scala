object PrimeFactors {
  def factors(x: Long): List[Long] = {
    def isPrime(n: Long): Boolean = !((2l to n-1) exists (n % _ == 0))
    def isPrimeFactor(n: Long, x: Long): Boolean = x % n == 0 && isPrime(n)

    if (x <= 1) List()
    else {
      val headPrime = Stream.from(2).find(i => isPrimeFactor(i, x)).get
      headPrime :: factors(x / headPrime)
    }
  }
}
