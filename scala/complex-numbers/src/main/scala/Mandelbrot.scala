import scala.collection.mutable.ListBuffer

object Mandelbrot {
  def mandelBrotElem(r: Double, i: Double): Int = {
    var z = ComplexNumber(0,0)
    val c = ComplexNumber(r,i)
    var n = 0
    while (z.abs < 2 && n < maxIter) {
      z = z * z + c
      n += 1
    }

    n
  }


  val mandelBrot = ComplexNumber
  val maxIter = 100

  val real = -2d to 1d by 0.01d
  val imaginary = 1d to -1d by -0.01d

  imaginary.map(i => real.map(r => mandelBrotElem(r, i)))

}
