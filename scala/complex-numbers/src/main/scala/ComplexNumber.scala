case class ComplexNumber(real: Double = 0.0, imaginary: Double = 0.0) {

  override def toString: String = s"$real + ${imaginary}i"

  def unary_- : ComplexNumber= ComplexNumber(-real, -imaginary)

  def conjugate: ComplexNumber = ComplexNumber(real, -imaginary)

  def -(that: ComplexNumber): ComplexNumber = {
    this + (-that)
  }

  def +(d: Double): ComplexNumber = {
    ComplexNumber(this.real + d, this.imaginary)
  }

  def +(that: ComplexNumber): ComplexNumber = {
    ComplexNumber(this.real + that.real, this.imaginary + that.imaginary)
  }

  def *(d: Double): ComplexNumber = this *  ComplexNumber(real = d)

  def *(that: ComplexNumber): ComplexNumber = {
      val reMultiple = ComplexNumber(real = this.real * that.real) +  ComplexNumber(imaginary = this.real * that.imaginary)
      val imMultiple = ComplexNumber(imaginary = this.imaginary * that.real) +  -ComplexNumber(real = this.imaginary * that.imaginary)
    reMultiple + imMultiple
    }

  def /(d: Double): ComplexNumber = ComplexNumber(this.real / d, this.imaginary / d)

  def /(that: ComplexNumber): ComplexNumber = {
    val nom = this * that.conjugate
    val demon = that * that.conjugate
    nom / demon.real
  }

  def abs: Double = math.sqrt((this.real * this.real) + (this.imaginary * this.imaginary))

  def square: ComplexNumber = this * this

}

object ComplexNumber {

  def exp(that: ComplexNumber): ComplexNumber = {
    ComplexNumber(math.cos(that.imaginary), math.sin(that.imaginary)) * math.exp(that.real)
  }

}
