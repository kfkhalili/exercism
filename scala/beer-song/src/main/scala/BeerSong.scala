object BeerSong {
  def recite(start: Int, numTimes: Int): String =  {
    def numberOrNoMore(number: Int): String = if (number > 0) number.toString else "no more"

    def singularOrPlural(number: Int): String = if (number > 1 || number == 0) "bottles" else "bottle"

    def itOrOne(number: Int): String = if (number == 1) "it" else "one"

    def say(number: Int): String = {
      s"$number ${singularOrPlural(start)} of beer on the wall, $number ${singularOrPlural(start)} of beer.\n" +
        s"Take ${itOrOne(start)} down and pass it around, " +
        s"${numberOrNoMore(start - 1)} ${singularOrPlural(start - 1)} of beer on the wall.\n"
    }

    def finish: String = "No more bottles of beer on the wall, no more bottles of beer.\n" +
      "Go to the store and buy some more, 99 bottles of beer on the wall.\n"

    if (start == 0) finish
    else if (numTimes == 1) say(start)
    else say(start) + "\n" + recite(start - 1, numTimes - 1)
  }
}
