object FoodChain {
  val animalMap = Map(
    "" -> "",
    "fly" -> "I don't know why she swallowed the fly. Perhaps she'll die.\n\n",
    "spider" -> "wriggled and jiggled and tickled inside her.\n",
    "bird" -> "How absurd to swallow a bird!\n",
    "cat" -> "Imagine that, to swallow a cat!\n",
    "dog" -> "What a hog, to swallow a dog!\n",
    "goat" -> "Just opened her throat and swallowed a goat!\n",
    "cow" -> "I don't know how she swallowed a cow!\n",
    "horse" -> "She's dead, of course!\n\n"
  )

  val animalName = Map(0 -> "", 1 -> "fly", 2 -> "spider", 3 -> "bird", 4 -> "cat",
    5 -> "dog", 6 -> "goat", 7 -> "cow", 8 -> "horse")

  def itOrThatSpider(idx: Int): String = {
    if(idx == 2 && animalName(idx) == "spider") "It "
    else if (animalName(idx) == "spider") "that "
    else ""
  }

  def followUp(idx: Int): String = {
    def utter = { s"She swallowed the ${animalName(idx)} to catch the ${animalName(idx-1)}" + (
      if (animalName(idx - 1) == "spider") " that " + animalMap("spider")
      else ".\n"
      )
    }
    if (idx == 1 || idx == 8) itOrThatSpider(idx) + animalMap(animalName(idx))
    else if (idx > 1 && idx < 8) utter + followUp(idx - 1)
    else ""
  }

  def recite(start: Int, end: Int): String = {
    def say(idx: Int): String = {
      s"I know an old lady who swallowed a ${animalName(idx)}.\n" +
        (
      if (idx == 1 || idx == 8) animalMap(animalName(idx))
      else itOrThatSpider(idx) + animalMap(animalName(idx)) + followUp(idx)
          )

    }

    if (start < end) say(start) + recite(start + 1, end)
    else say(start)
  }
}
