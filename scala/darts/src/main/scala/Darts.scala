object Darts {
  def score(x: Double, y: Double): Int = {
    if (x > 10 || y > 10) 0 // outside of bounds
    else if (x > 5 || y > 5) 1
    else if (x >= 1 || y >= 1 ) 5 // catches condition x or y == 5
    else 10
  }
}
