object Bob {

  def isYelling(s: String): Boolean = s.toUpperCase == s && s.toUpperCase != s.toLowerCase
  def usesWords(c: Char): Boolean = c.isLetterOrDigit || c == '?' || c == '!'

  def response(statement: String): String = {
    val onlySpeech = statement.filter(usesWords)
    if (onlySpeech.isEmpty) "Fine. Be that way!"
    else if (isYelling(onlySpeech)) onlySpeech.last match {
      case '?' => "Calm down, I know what I'm doing!"
      case _ => "Whoa, chill out!"
    }
    else onlySpeech.last match {
      case '?' => "Sure."
      case _ => "Whatever."
    }
  }
}
