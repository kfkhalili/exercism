object NthPrime {
  val primes: Stream[Int] = { 2 #:: Stream.from(3, 2) // all primes after 2 are odd so we increment in 2s.
    .filter(i => // given numbers, filter numbers i
    primes.takeWhile(j => j*j <= i) // take numbers j where j*j <= i
      .forall(k => i % k != 0)) // for all numbers which dont divide and are prime
  }

  def prime(n: Int): Option[Int] = {
    if (n < 1) None
    else {
      Some(primes(n - 1))
    }
  }
}
