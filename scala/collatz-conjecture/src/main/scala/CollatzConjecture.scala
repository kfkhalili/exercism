object CollatzConjecture {
  def steps(x: Int): Option[Int] = {
    def stepper(x: Int, step: Int): Option[Int] = {
      if (x < 1) None
      else if (x == 1) Some(step)
      else if (x % 2 == 0) stepper(x / 2, step + 1)
      else stepper((3 * x) + 1, step + 1)
    }

    stepper(x, 0)
  }
}
