
object BracketPush {
  val parens:Map[Char, Char] = Map('(' -> ')', '{' -> '}', '[' -> ']')

  def balance(chars: List[Char]): Boolean = {
    def inner(acc: Int, chars: List[Char], open: List[Char]): Boolean = {
      if (chars.isEmpty) acc match {
        case 0 => true
        case _ => false
      }
      else chars.head match {
        case c if parens.keySet(c) => inner(acc + 1, chars.tail, c :: open)
        case c if parens.values.toSet(c) => acc match {
          case 0 => false
          case _ =>
            if(parens.find(_._2 == c).get._1 == open.head) inner(acc - 1, chars.tail, open.tail)
            else false
        }
        case _ => inner(acc, chars.tail, open)
      }
    }

    inner(0, chars, List())
  }


  def isPaired(s: String): Boolean = {
    balance(s.toList)
  }
}
