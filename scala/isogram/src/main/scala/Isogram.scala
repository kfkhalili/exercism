object Isogram {
  def isIsogram(word: String): Boolean = {
    def notDashOrSpace(c: Char): Boolean = c != '-' && c != ' '
    val dashSpaceLessWord = word.filter(notDashOrSpace)

    dashSpaceLessWord.toLowerCase.toSet.size == dashSpaceLessWord.length
  }
}
