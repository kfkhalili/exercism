object Sieve {
  def primes(limit: Int): List[Int] = {
    val emptyRange = 0 until 0
    val range = if (limit >= 2) 2 to limit else emptyRange
    if (range.nonEmpty)
      (1 to range.length).map(idx =>
        range.filter(x => (x > idx) && (x % idx == 0))
          .toSet)
        .reduce(_ diff _)
        .toList
        .sorted
    else List()
  }
}
