case class Triangle(s1: Double, s2: Double, s3: Double) {
  def isValid: Boolean = (s1 > 0 && s2 > 0 && s3 > 0) &&
    (s1 + s2 > s3 && s2 + s3 > s1 && s1 + s3 > s2)

  def equilateral: Boolean = if(isValid) s1 == s2 && s2 == s3 else false
  def isosceles: Boolean = if(isValid) s1 == s2 || s1 == s3 || s2 == s3 else false
  def scalene: Boolean = if(isValid) s1 != s2 && s2 != s3 else false
}
