object SumOfMultiples {
  def sum(factors: Set[Int], limit: Int): Int = {
    def getMultiples(factor: Int): Seq[Int] = {
      (1 to limit).map(_ * factor).filter(_ < limit)
    }

    factors.flatMap(getMultiples).sum
  }
}

