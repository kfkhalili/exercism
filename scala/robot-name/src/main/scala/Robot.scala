
object NameGenerator {
  // Random generator
  val random = new scala.util.Random

  // Generate a random string of length n from the given alphabet
  def randomString(alphabet: String)(n: Int): String =
    Stream.continually(random.nextInt(alphabet.length)).map(alphabet).take(n).mkString

  // Generate a random alphanumeric string of length n
  def randomAlphaString(n: Int): String =
    randomString("ABCDEFGHIJKLMNOPQRSTUVWXYZ".toSet.mkString)(n)

  // Generate a random alphanumeric string of length n
  def randomNumericString(n: Int): String =
    randomString(System.currentTimeMillis.toString)(n)

  def genName: String = randomAlphaString(2) + randomNumericString(3)
}

object NameTracker {
  val allNumbers: Seq[String] = (0 to 999).map("%03d".format(_))

  val allNames: Iterator[String] = (for {
    firstLetter <- 'A' to 'Z'
    secondLetter <- 'A' to 'Z'
    number <- allNumbers
  } yield s"$firstLetter$secondLetter$number")
    .toIterator
}


class Robot {
  var name: String = NameTracker.allNames.next

  def reset(): Unit = {
    name = NameTracker.allNames.next
  }
}
