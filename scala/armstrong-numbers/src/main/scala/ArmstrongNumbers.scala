object ArmstrongNumbers {
  def isArmstrongNumber(x: Int): Boolean = {
    val numDigits: Int = s"$x".length

    val armstrongMapping = x.toString.map(_.asDigit)
      .map(d => math.pow(d,numDigits)).sum

    x == armstrongMapping
  }
}
