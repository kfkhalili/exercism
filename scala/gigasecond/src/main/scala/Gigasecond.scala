import java.time.{LocalDate, LocalDateTime, ZoneId, ZoneOffset}
import java.time.format.DateTimeFormatter
import java.time.Instant



object Gigasecond {
  val gigasecond = 1000000000L

  private def dateTime(str: String): LocalDateTime =
    LocalDateTime.from(DateTimeFormatter.ISO_DATE_TIME.parse(str))
  private def date(str: String): LocalDate =
    LocalDate.from(DateTimeFormatter.ISO_DATE.parse(str))

  def add(startDate: LocalDate): LocalDateTime = {
    val zoneId = ZoneId.ofOffset("UTC", ZoneOffset.ofHours(0))
    val epochMs = (startDate.atStartOfDay(zoneId).toEpochSecond + gigasecond) * 1000
    LocalDateTime.ofInstant(Instant.ofEpochMilli(epochMs), zoneId)
  }

  def add(startDateTime: LocalDateTime): LocalDateTime = {
    startDateTime.plusSeconds(gigasecond)
  }
}
