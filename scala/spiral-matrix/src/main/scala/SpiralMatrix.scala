case class SpiralMatrix() {

}


object SpiralMatrix {
  type Matrix = List[List[Int]]

  // BULLSHIT DOES NOT WORK
  def spiralMatrix(rows: Int): Matrix = {
    def dropOuterRight(matrix: Matrix): Matrix = matrix.tail.map(_.dropRight(1))

    def rotatedView(i: Int)(matrix: Matrix): Matrix = {
      val concat = matrix.flatten.drop(i) ++ matrix.flatten.take(i)
      concat.grouped(matrix.head.length).toList
    }

    def rotatedSeq(i: Int)(seq: Seq[Int]): Seq[Int] = seq.drop(i) ++ seq.take(i)

    val range = (1 to rows * rows).toList
    val head = range.take(math.sqrt(range.length).toInt)
    val tail = range.drop(math.sqrt(range.length).toInt)



    val matrix = (1 to rows * rows).grouped(rows).map(_.toList).toList
    matrix.head :: rotatedView(0)(matrix.tail)
  }
}

