object SpiralMatrixPerfect {
  def run(t:Int,s:Int,ls:List[Int]): List[Int] = t match {
    case 0 => ls
    case _ => run(t-s*2, s-1, ls ::: List(s,s))
  }

  def dist(numbers:List[Int], slides: List[Int], turns: List[List[Int]]): List[List[Int]] = numbers match {
    case Nil => turns
    case _ => dist(numbers.drop(slides.head), slides.tail, turns :+ numbers.take(slides.head))
  }

  def spiralIt(dirs:Seq[Char],turns:List[List[Int]],matrix:Array[Array[Int]])(b:(Int,Int),f:(Int,Int)): Array[Array[Int]] = turns match {
    case Nil => matrix
    case _ =>
      val (mn, mx) = b   // boundaries
    val (fmn, fmx) = f // fix cols | rows
    val fn = spiralIt (dirs.tail, turns.tail, matrix) _

      dirs.head match {
        case 'r' =>
          (mn to mx).toList zip turns.head foreach (v => matrix(fmn)(v._1) = v._2)
          fn ((mn + 1, mx), (fmn,fmx))
        case 'd' =>
          (mn to mx).toList zip turns.head foreach (v => matrix(v._1)(fmx) = v._2)
          fn ((mn - 1, mx - 1), (fmn, fmx))
        case 'l' =>
          (mx to mn by -1).toList zip turns.head foreach (v => matrix(fmx)(v._1) = v._2)
          fn ((mn + 1, mx), (fmn, fmx))
        case 'u' =>
          (mx to mn by -1).toList zip turns.head foreach (v => matrix(v._1)(fmn) = v._2)
          fn ((mn, mx), (fmn + 1, fmx - 1))
      }
  }

  def spiralMatrix(size: Int): List[List[Int]] = size match {
    case 0 => Nil
    case 1 => List(List(1))
    case _ => {
      val strides = run(size*(size-1),size-1,List(size))
      val turns = dist((1 to size*size).toList, strides, List())
      val dirs = Stream.continually("rdlu".toStream).flatten.take(turns.size).toVector
      spiralIt(dirs, turns, Array.ofDim[Int](size,size))((0,size-1),(0,size-1)).toList.map(_.toList)        }
  }
}