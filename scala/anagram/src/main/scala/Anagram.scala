object Anagram {
  def findAnagrams(anagramTemplate: String, l: List[String]): List[String] = {
    def countChar(s: String, c: Char): (Char, Int) = (c, s.toLowerCase.count(_ == c))

    def areAnagram(s1: String, s2: String): Boolean = {
      s1.length == s2.length &&
        s1.toLowerCase.map(countChar(s1, _)).toSet == s2.toLowerCase.map(countChar(s2, _)).toSet &&
        s1.toLowerCase != s2.toLowerCase
    }

    l.filter(areAnagram(anagramTemplate, _))
  }
}
